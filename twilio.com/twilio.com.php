<?php
include_once("base.php");
include_once("includes/json.php");

class clsTwilio_comAPI extends clsTelephonyAPI
{
	var $strAccountSID;

	public function __construct($arrOptions = array())
	{
		global $arrRequestOptionsDefault;
		
		$this->strService = "twilio.com";
		$this->arrRequestOptions = $arrRequestOptionsDefault;
		$this->arrRequestOptions["strAuthenticationUsername"] = $arrOptions["strAuthAccount"] ? trim($arrOptions["strAuthAccount"]) : $this->arrRequestOptions["strAuthenticationUsername"];
		$this->arrRequestOptions["strAuthenticationPassword"] = $arrOptions["strAuthPassword"] ? trim($arrOptions["strAuthPassword"]) : $this->arrRequestOptions["strAuthenticationPassword"];
		$this->arrRequestOptions["intTimeOutSeconds"] = $arrOptions["intTimeOutSeconds"] ? $arrOptions["intTimeOutSeconds"] : $this->arrRequestOptions["intTimeOutSeconds"];
		$this->arrRequestOptions["blnVerifySSL"] = $arrOptions["blnVerifySSL"] ? $arrOptions["blnVerifySSL"] : $this->arrRequestOptions["blnVerifySSL"];
		
		$this->arrRequestOptions["urlOutboundCallTwiml"] = $arrOptions["urlOutboundCallTwiml"] ? $arrOptions["urlOutboundCallTwiml"] : null;
		$this->arrRequestOptions["blnOutboundCallHangupIfMachine"] = $arrOptions["blnOutboundCallHangupIfMachine"] ? $arrOptions["blnOutboundCallHangupIfMachine"] : false;
		$this->arrRequestOptions["blnOutboundCallRecordCompletely"] = $arrOptions["blnOutboundCallRecordCompletely"] ? $arrOptions["blnOutboundCallRecordCompletely"] : false;
		$this->arrRequestOptions["strDefaultFromNumber"] = $arrOptions["strDefaultFromNumber"] ? trim($arrOptions["strDefaultFromNumber"]) : null;
		
		$this->strAccountSID = $arrOptions["strAccountSID"] ? trim($arrOptions["strAccountSID"]) : $this->arrRequestOptions["strAuthenticationUsername"];
		$this->strBaseUrl = $arrOptions["strBaseUrl"] ? trim($arrOptions["strBaseUrl"]) : "https://api.twilio.com/2010-04-01/";
		
		if(substr(trim($this->strBaseUrl), strlen(trim($this->strBaseUrl)) - 1, 1) === "/")
			$this->strBaseUrl = substr(trim($this->strBaseUrl), 0, strlen(trim($this->strBaseUrl)) - 1);
		else
			$this->strBaseUrl = trim($this->strBaseUrl);
		
		return $this->blnConnect($arrOptions);
	}
	
	public function __destruct()
	{
		return $this->blnDisconnect();
	}
	
	/*
	 * Preparing connection to the Twilio service, setup variables.
	 * 
	 * arrOptions can currently, but not necessarily, have the following indexes, with defaults if not set:
	 *  strAuthAccount = ""
	 *  strAuthPassword = ""
	 *  strAccountSID = ""
	 *  intTimeOutSeconds = 30
	 */
	public function blnConnect($arrOptions = array())
	{
		return true;
	}
	
	public function blnDisconnect()
	{
		return true;
	}

	public function blnIsSID($strSID)
	{
		$strService = trim(strtok($strSID, '*'));
		if(strtolower($strService) === strtolower(trim($this->strService)))
		{
			if(strlen($strSID) - (strlen($strService) + 1) > 0)
				return true;
			else
				return false;
		}
		else
			return false;
	}
	
	
	public function strAdviceCheck($blnConnect = false)
	{
		$strMinUrl = "api.twilio.com/2010-04-01";
		
		if($this->strBaseUrl === "")
			return "The Base URL is not set. Check the options array given to the constructor, or any call to blnConnect?";
		elseif(substr($this->strBaseUrl, 0, strlen($strMinUrl)) != $strMinUrl && 
				substr($this->strBaseUrl, 7, strlen($strMinUrl)) != $strMinUrl && 
				substr($this->strBaseUrl, 8, strlen($strMinUrl)) != $strMinUrl)
			return "The service call does not seem to be calling a compatible url. Current minimum url is '{$strMinUrl}', " .
				"optionally preceeded with 'http://' or 'https://'.";
		elseif($this->strAccountSID === "")
			return "The default twilio account to be used, is not set. Check the options array given to the constructor, or " .
				"any call to blnConnect? The index strAccountSID must be set correctly.";
		elseif(substr($this->strAccountSID,0,2) !== "AC" || 
				strlen($this->strAccountSID) != 34 || 
				!ctype_alnum($this->strAccountSID))
			return "The default twilio account to be used, is not set correctly. The account SID must start with 'AC', followed by a 32 long alphanumeric string.";
		elseif(substr($this->arrRequestOptions["strAuthenticationUsername"],0,2) !== "AC" || 
				strlen($this->arrRequestOptions["strAuthenticationUsername"]) != 34 || 
				!ctype_alnum($this->arrRequestOptions["strAuthenticationUsername"]))
			return "The twilio account to be authenticated with, is not set correctly. The account SID must start with 'AC', followed by a 32 long alphanumeric string.";
		elseif(strlen($this->arrRequestOptions["strAuthenticationPassword"]) != 32 || 
				!ctype_alnum($this->arrRequestOptions["strAuthenticationPassword"]))
			return "The authentication password token to be used, is not set correctly. The password must be 32 long alphanumeric string.";
		elseif($blnConnect && !($resConnection = @fsockopen("www.google.com", 80)))
			return "A connection with the internet could not be established. Check that the server has open and direct access.";
		elseif($blnConnect && !($resConnection = @fsockopen("www.google.com", 443)))
			return "A connection can be established, but SSL are failing. Check that this server has SSL installed server-side.";
		elseif($blnConnect && ($arrServiceInfo = $this->arrServiceInfo()) === false)
			return "Requesting information from the service failed. Is the service up and running, and are you providing correct credentials?";
		elseif($blnConnect && !$arrServiceInfo["blnStatus"])
			return "Information with the account can be requested, but the account is inactive. Check the status of the account by logging into twilio.";
		else
			return "Your service is ready to be used for calls and SMSs.";
		
	}
	
	public function blnServiceReady($blnConnect = false)
	{
		$strMinUrl = "api.twilio.com/2010-04-01";
		
		if(		($this->strBaseUrl === "") ||
				
				(substr($this->strBaseUrl, 0, strlen($strMinUrl)) != $strMinUrl && 
				substr($this->strBaseUrl, 7, strlen($strMinUrl)) != $strMinUrl && 
				substr($this->strBaseUrl, 8, strlen($strMinUrl)) != $strMinUrl) ||
				
				($this->strAccountSID === "") ||
				
				(substr($this->strAccountSID,0,2) !== "AC" || 
				strlen($this->strAccountSID) != 34 || 
				!ctype_alnum($this->strAccountSID)) ||
				
				(substr($this->arrRequestOptions["strAuthenticationUsername"],0,2) !== "AC" || 
				strlen($this->arrRequestOptions["strAuthenticationUsername"]) != 34 || 
				!ctype_alnum($this->arrRequestOptions["strAuthenticationUsername"])) ||
				
				(strlen($this->arrRequestOptions["strAuthenticationPassword"]) != 32 || 
				!ctype_alnum($this->arrRequestOptions["strAuthenticationPassword"]))
		)
			return false;
		else
			return true;
	}
	
	
	public function arrServiceInfo()
	{
		$strUrl = "{$this->strBaseUrl}/Accounts/{$this->strAccountSID}.json";
		if(($arrResponse = $this->arrTwilioRequest($strUrl, $this->arrRequestOptions)) === false)
			return false;
		else
			return array(
					"strSID" => $this->strEncodeSID($arrResponse["sid"]),
					"strFriendlyName" => $arrResponse["friendly_name"],
					"strStatus" => $arrResponse["status"],
					"blnStatus" => (trim($arrResponse["status"]) == "active"),
					"strType" => trim($arrResponse["type"]),
					"blnOutboundCallingSupported" => true,
					"blnInboundCallingSupported" => true,
					"blnOutboundSmsSupported" => true,
					"blnInboundSmsSupported" => true,
					"intCreditUsdCents" => null,
					"datCreated" => date('Y-m-d H:i:s', strtotime(trim($arrResponse["date_created"]))),
					"datModified" => date('Y-m-d H:i:s', strtotime(trim($arrResponse["date_updated"]))));
	}
	
	
	
	
	public function arrSendMessage($strMessage = null, $strTo = null, $strFrom = null)
	{
		if(!trim($strMessage))
			$this->strLastError = "A message was not provided.";
		elseif(!trim($strTo))
			$this->strLastError = "A receiving number was not provided.";
		elseif(!trim($strFrom) && !$this->arrRequestOptions["strDefaultFromNumber"])
			$this->strLastError = "A sending number was not provided.";
		else
		{
			if(!trim($strFrom))
				$strFrom = $this->arrRequestOptions["strDefaultFromNumber"];
			
			$strUrl = "{$this->strBaseUrl}/Accounts/{$this->strAccountSID}/SMS/Messages.json";
			$arrPostFields = array(
					"To" => $strTo,
					"From" => $strFrom,
					"Body" => $strMessage);
			if(($arrResponse = $this->arrTwilioRequest($strUrl, $this->arrRequestOptions, $arrPostFields)) === false)
				return false;
			else
				return array(
						"strSID" => $this->strEncodeSID($arrResponse["sid"]),
						"intPriceCents" => is_null($arrResponse["price"]) ? null : abs((float)trim($arrResponse["price"]) * 100),
						"strPriceUnit" => strtolower(trim($arrResponse["price_unit"])),
						"strStatus" => strtolower(trim($arrResponse["status"])),
						"blnIsStateFinal" => $this->blnIsStatusFinal(trim($arrResponse["status"])),
						"datSent" => date('Y-m-d H:i:s', strtotime(trim($arrResponse["date_sent"]))),
						"datCreated" => date('Y-m-d H:i:s', strtotime(trim($arrResponse["date_created"]))),
						"datModified" => date('Y-m-d H:i:s', strtotime(trim($arrResponse["date_updated"]))));
		}
		return false;
	}
	
	public function arrCheckMessage($strMessageSID = null)
	{
		if(!$this->blnIsSID($strMessageSID))
			$this->strLastError = "An incorrect message identifier is provided.";
		else
		{
			$strMessageSID = $this->strDecodeSID($strMessageSID);
			$strUrl = "{$this->strBaseUrl}/Accounts/{$this->strAccountSID}/SMS/Messages/{$strMessageSID}.json";
			if(($arrResponse = $this->arrTwilioRequest($strUrl, $this->arrRequestOptions, null)) === false)
				return false;
			else
				return array(
						"strSID" => $this->strEncodeSID($arrResponse["sid"]),
						"intPriceCents" => is_null($arrResponse["price"]) ? null : abs((float)trim($arrResponse["price"]) * 100),
						"strPriceUnit" => strtolower(trim($arrResponse["price_unit"])),
						"strStatus" => strtolower(trim($arrResponse["status"])),
						"blnIsStateFinal" => $this->blnIsStatusFinal(trim($arrResponse["status"])),
						"datSent" => date('Y-m-d H:i:s', strtotime(trim($arrResponse["date_sent"]))),
						"datCreated" => date('Y-m-d H:i:s', strtotime(trim($arrResponse["date_created"]))),
						"datModified" => date('Y-m-d H:i:s', strtotime(trim($arrResponse["date_updated"]))));
		}
		return false;
	}
	
	public function arrMakeCall($strTo = null, $strFrom = null, $arrAdditionalOptions = null)
	{
		if(!trim($strTo))
			$this->strLastError = "A receiving number was not provided.";
		if(!trim($strFrom) && !$this->arrRequestOptions["strDefaultFromNumber"])
			$this->strLastError = "A sending number was not provided.";
		else
		{
			if(!trim($strFrom))
				$strFrom = $this->arrRequestOptions["strDefaultFromNumber"];
			
			$arrOptions = $this->arrRequestOptions;
			if($arrAdditionalOptions !== null)
				foreach($arrAdditionalOptions as $strOption => $strValue)
					$arrOptions[$strOption] = $strValue;
			
			$strUrl = "{$this->strBaseUrl}/Accounts/{$this->strAccountSID}/Calls.json";
			$arrPostFields = array(
					"To" => $strTo,
					"From" => $strFrom,
					"Url" => $arrOptions["urlOutboundCallTwiml"],
					"IfMachine" => $arrOptions["blnOutboundCallHangupIfMachine"] ? "Hangup" : "Continue",
					"Timeout" => (string)$arrOptions["intTimeOutSeconds"]);
			if($arrOptions["blnOutboundCallRecordCompletely"])
				$arrPostFields["Record"] = "true";
			if(($arrResponse = $this->arrTwilioRequest($strUrl, $this->arrRequestOptions, $arrPostFields)) === false)
				return false;
			else
				return array(
						"strSID" => $this->strEncodeSID($arrResponse["sid"]),
						"strFromNumberSID" => $this->strEncodeSID($arrResponse["phone_number_sid"]),
						"datCallStart" => date('Y-m-d H:i:s', strtotime(trim($arrResponse["start_time"]))), 
						"datCallEnd" => date('Y-m-d H:i:s', strtotime(trim($arrResponse["end_time"]))), 
						"intDurationSeconds" => (int)trim($arrResponse["duration"]),
						"intPriceCents" => is_null($arrResponse["price"]) ? null : abs((float)trim($arrResponse["price"]) * 100),
						"strPriceUnit" => strtolower(trim($arrResponse["price_unit"])),
						"strResponderType" => trim($arrResponse["answered_by"]),
						"strForwardedFrom" => $arrResponse["forwarded_from"],
						"strAnnotation" => $arrResponse["annotation"],
						"strCallerName" => $arrResponse["caller_name"],
						"strStatus" => strtolower(trim($arrResponse["status"])),
						"blnIsStateFinal" => $this->blnIsStatusFinal(trim($arrResponse["status"])),
						"datCreated" => date('Y-m-d H:i:s', strtotime(trim($arrResponse["date_created"]))),
						"datModified" => date('Y-m-d H:i:s', strtotime(trim($arrResponse["date_updated"]))));
		}
		return false;
	}
	
	public function arrCheckCall($strCallSID = null)
	{
		if(!$this->blnIsSID($strCallSID))
			$this->strLastError = "An incorrect call identifier is provided.";
		else
		{
			$strCallSID = $this->strDecodeSID($strCallSID);
			$strUrl = "{$this->strBaseUrl}/Accounts/{$this->strAccountSID}/Calls/{$strCallSID}.json";
			if(($arrResponse = $this->arrTwilioRequest($strUrl, $this->arrRequestOptions, null)) === false)
				return false;
			else
				return array(
						"strSID" => $this->strEncodeSID($arrResponse["sid"]),
						"strFromNumberSID" => $this->strEncodeSID($arrResponse["phone_number_sid"]),
						"datCallStart" => date('Y-m-d H:i:s', strtotime(trim($arrResponse["start_time"]))), 
						"datCallEnd" => date('Y-m-d H:i:s', strtotime(trim($arrResponse["end_time"]))), 
						"intDurationSeconds" => (int)trim($arrResponse["duration"]),
						"intPriceCents" => is_null($arrResponse["price"]) ? null : abs((float)trim($arrResponse["price"]) * 100),
						"strPriceUnit" => strtolower(trim($arrResponse["price_unit"])),
						"strResponderType" => trim($arrResponse["answered_by"]),
						"strForwardedFrom" => $arrResponse["forwarded_from"],
						"strAnnotation" => $arrResponse["annotation"],
						"strCallerName" => $arrResponse["caller_name"],
						"strStatus" => strtolower(trim($arrResponse["status"])),
						"blnIsStateFinal" => $this->blnIsStatusFinal(trim($arrResponse["status"])),
						"datCreated" => date('Y-m-d H:i:s', strtotime(trim($arrResponse["date_created"]))),
						"datModified" => date('Y-m-d H:i:s', strtotime(trim($arrResponse["date_updated"]))));
		}
		return false;
	}
	
	public function arrGetRecordings($strCallSID = null)
	{
		if(!$this->blnIsSID($strCallSID))
			$this->strLastError = "An incorrect recording identifier is provided.";
		else
		{
			$strCallSID = $this->strDecodeSID($strCallSID);
			$strUrl = "{$this->strBaseUrl}/Accounts/{$this->strAccountSID}/Calls/{$strCallSID}/Recordings.json?PageSize=500";
			if(($arrResponse = $this->arrTwilioRequest($strUrl, $this->arrRequestOptions, null)) === false)
				return false;
			else
			{
				$arrReturn = array();
				foreach($arrResponse["recordings"] as $arrRecording)
				{
					$arrReturn[] = array(
						"strSID" => $this->strEncodeSID($arrRecording["sid"]),
						"intDurationSeconds" => (int)trim($arrRecording["duration"]),
						"datCreated" => date('Y-m-d H:i:s', strtotime(trim($arrRecording["date_created"]))),
						"datModified" => date('Y-m-d H:i:s', strtotime(trim($arrRecording["date_updated"]))));
				}
				return $arrReturn;
			}
		}
		return false;
	}
	
	public function strCreateRecordingUrl($strRecordingSID = null)
	{
		if(!$this->blnIsSID($strCallSID))
			$this->strLastError = "An incorrect recording identifier is provided.";
		else
		{
			$strRecordingSID = $this->strDecodeSID($strRecordingSID);
			$strUrl = "{$this->strBaseUrl}/Accounts/{$this->strAccountSID}/Recordings/{$strRecordingSID}.mp3";
			if(substr($strUrl, 0, 5) == "https")
				$strUrl = "http" . substr($strUrl, 5);
			return $strUrl;
		}
		return false;
	}
	
	
	
	public function arrGetOutboundCallNumbers()
	{
		$strCountryIso = "US"; //TODO: For twilio, this is only US or CA. How we code these choices...
		$strUrl = "{$this->strBaseUrl}/Accounts/{$this->strAccountSID}/AvailablePhoneNumbers/{$strCountryIso}/Local.json";
		
		if(($arrResponse = $this->arrTwilioRequest($strUrl, $this->arrRequestOptions, null)) === false)
			return false;
		elseif(($arrReturn = $this->arrGetInboundCallNumbers()) === false)
			return false;
		else
		{
			foreach($arrResponse["available_phone_numbers"] as $arrNumber)
			{
				//TODO: Can't determine capabilities from the api, assuming both
				$arrReturn[] = array(
							"strSID" => $this->strEncodeSID($arrNumber["sid"]),
							"strNumber" => trim($arrNumber["phone_number"]),
							"blnCallerLookupId" => false,
							"blnOutboundCallingSupported" => true,
							"blnInboundCallingSupported" => false,
							"blnOutboundSmsSupported" => true,
							"blnInboundSmsSupported" => false,
							"datCreated" => date('Y-m-d H:i:s', strtotime(trim($arrNumber["date_created"]))),
							"datModified" => date('Y-m-d H:i:s', strtotime(trim($arrNumber["date_updated"]))));
			}
			return $arrReturn;
		}
	}
	
	public function arrGetInboundCallNumbers()
	{
		$strUrl = "{$this->strBaseUrl}/Accounts/{$this->strAccountSID}/IncomingPhoneNumbers.json";
		if(($arrResponse = $this->arrTwilioRequest($strUrl, $this->arrRequestOptions, null)) === false)
			return false;
		else
		{
			$arrReturn = array();
			foreach($arrResponse["incoming_phone_numbers"] as $arrNumber)
			{
				if($arrNumber["capabilities"]["voice"])
					$arrReturn[] = array(
							"strSID" => $this->strEncodeSID($arrNumber["sid"]),
							"strNumber" => trim($arrNumber["phone_number"]),
							"blnCallerLookupId" => $arrNumber["voice_caller_id_lookup"],
							"blnOutboundCallingSupported" => $arrNumber["capabilities"]["voice"],
							"blnInboundCallingSupported" => $arrNumber["capabilities"]["voice"],
							"blnOutboundSmsSupported" => $arrNumber["capabilities"]["sms"],
							"blnInboundSmsSupported" => $arrNumber["capabilities"]["sms"],
							"datCreated" => date('Y-m-d H:i:s', strtotime(trim($arrNumber["date_created"]))),
							"datModified" => date('Y-m-d H:i:s', strtotime(trim($arrNumber["date_updated"]))));
			}
			return $arrReturn;
		}
	}

	public function arrGetOutboundSmsNumbers()
	{
		$strCountryIso = "US"; //TODO: For twilio, this is only US or CA. How we code these choices...
		$strUrl = "{$this->strBaseUrl}/Accounts/{$this->strAccountSID}/AvailablePhoneNumbers/{$strCountryIso}/Local.json";
		
		if(($arrResponse = $this->arrTwilioRequest($strUrl, $this->arrRequestOptions, null)) === false)
			return false;
		elseif(($arrReturn = $this->arrGetInboundCallNumbers()) === false)
			return false;
		else
		{
			foreach($arrResponse["available_phone_numbers"] as $arrNumber)
			{
				//TODO: Can't determine capabilities from the api, assuming both
				$arrReturn[] = array(
							"strSID" => $this->strEncodeSID($arrNumber["sid"]),
							"strNumber" => trim($arrNumber["phone_number"]),
							"blnCallerLookupId" => false,
							"blnOutboundCallingSupported" => true,
							"blnInboundCallingSupported" => false,
							"blnOutboundSmsSupported" => true,
							"blnInboundSmsSupported" => false,
							"datCreated" => date('Y-m-d H:i:s', strtotime(trim($arrNumber["date_created"]))),
							"datModified" => date('Y-m-d H:i:s', strtotime(trim($arrNumber["date_updated"]))));
			}
			return $arrReturn;
		}
	}
	
	public function arrGetInboundSmsNumbers()
	{
		$strUrl = "{$this->strBaseUrl}/Accounts/{$this->strAccountSID}/IncomingPhoneNumbers.json";
		if(($arrResponse = $this->arrTwilioRequest($strUrl, $this->arrRequestOptions, null)) === false)
			return false;
		else
		{
			$arrReturn = array();
			foreach($arrResponse["incoming_phone_numbers"] as $arrNumber)
			{
				if($arrNumber["capabilities"]["sms"])
					$arrReturn[] = array(
							"strSID" => $this->strEncodeSID($arrNumber["sid"]),
							"strNumber" => trim($arrNumber["phone_number"]),
							"blnCallerLookupId" => $arrNumber["voice_caller_id_lookup"],
							"blnOutboundCallingSupported" => $arrNumber["capabilities"]["voice"],
							"blnInboundCallingSupported" => $arrNumber["capabilities"]["voice"],
							"blnOutboundSmsSupported" => $arrNumber["capabilities"]["sms"],
							"blnInboundSmsSupported" => $arrNumber["capabilities"]["sms"],
							"datCreated" => date('Y-m-d H:i:s', strtotime(trim($arrNumber["date_created"]))),
							"datModified" => date('Y-m-d H:i:s', strtotime(trim($arrNumber["date_updated"]))));
			}
			return $arrReturn;
		}
	}
	
	
	
	
	
	
	
	private function arrTwilioRequest($strUrl = null, $arrRequestOptions = null, $arrPostFields = null)
	{
		if(!$arrRequestOptions)
			$arrRequestOptions = $this->arrRequestOptions;
		
		if(!$strUrl)
			$this->strLastError = "No Twilio.com service url specified.";
		elseif(!$arrRequestOptions)
			$this->strLastError = "No request options specified.";
		elseif(($arrReponse = arrRequestJSON($strUrl, $arrRequestOptions, $arrPostFields)) === false)
			$this->strLastError = strRequestJSONError();
		elseif(!(intRequestJSONStatus() >= 200 && intRequestJSONStatus() < 300) && isset($arrReponse["message"]))
			$this->strLastError = "Twilio.com http ".intRequestJSONStatus().": {$arrReponse["message"]}";
		elseif(!(intRequestJSONStatus() >= 200 && intRequestJSONStatus() < 300))
			$this->strLastError = "The Twilio.com service returned http code ".intRequestJSONStatus()." (".strHttpCodeDescription(intRequestJSONStatus()).")";
		else
			return $arrReponse;
		return false;
	}
	
	
	private function blnIsStatusFinal($strResponseStatus)
	{
		switch(strtolower($strResponseStatus))
		{
			case "failed":
			case "no-answer":
			case "completed":
			case "sent":
				return true;
			default:
				return false;
		}
	}

}

