<?php
include_once("base.php");
include_once("includes/json.php");

class clsCallfire_comAPI extends clsTelephonyAPI
{
	var $objSoapClient = null;
	
	public function __construct($arrOptions = array())
	{
		global $arrRequestOptionsDefault;
		$this->strService = "callfire.com";
		$this->arrRequestOptions = $arrRequestOptionsDefault;
		$this->arrRequestOptions["strAuthenticationUsername"] = isset($arrOptions["strLogin"]) ? trim($arrOptions["strLogin"]) : $this->arrRequestOptions["strAuthenticationUsername"];
		$this->arrRequestOptions["strAuthenticationPassword"] = isset($arrOptions["strPassword"]) ? trim($arrOptions["strPassword"]) : $this->arrRequestOptions["strAuthenticationPassword"];
		$this->arrRequestOptions["intBroadcastID"] = isset($arrOptions["intBroadcastID"]) ? trim($arrOptions["intBroadcastID"]) : null;
		$this->arrRequestOptions["cnsSoapVersion"] = isset($arrOptions["cnsSoapVersion"]) ? round(trim($arrOptions["cnsSoapVersion"])) : SOAP_1_2;
		$this->arrRequestOptions["strDefaultFromNumber"] = isset($arrOptions["strDefaultFromNumber"]) ? trim($arrOptions["strDefaultFromNumber"]) : null;
		
		$this->strBaseUrl = isset($arrOptions["strBaseUrl"]) ? trim($arrOptions["strBaseUrl"]) : "https://callfire.com/api/1.0/wsdl/callfire-service-http-soap12.wsdl";
		
		
		return $this->blnConnect($arrOptions);
	}

	public function __destruct()
	{
		return $this->blnDisconnect();
	}
	
	public function blnConnect($arrOptions = array())
	{
		$this->objSoapClient = $this->objGetSoapClient();
		return true;
	}


	public function blnDisconnect()
	{
		return true;
	}
	
	
	
	
	public function strAdviceCheck($blnConnect = false)
	{
		if($this->strBaseUrl == "")
			return "The Base URL is not set. Check the options array given to the constructor, or any call to blnConnect?";
		elseif(!$this->arrRequestOptions["strAuthenticationUsername"])
			return "The callfire account or username to be used, is not set. Check the callfire config array? The index " .
				"strAuthenticationUsername must be set correctly.";
		elseif(!$this->arrRequestOptions["strAuthenticationPassword"])
			return "The callfire password is not set. Check the callfire config array? The index strAuthenticationPassword must be set correctly.";
		elseif(!$this->arrRequestOptions["cnsSoapVersion"])
			return "The soap version for the callfire soapclient is not set. Check the callfire config array? The index cnsSoapVersion must be set correctly.";
		elseif($blnConnect && ($objCallfire = objGetSoapClient()) === false)
			return "About the soap client: " . $this->strLastErrorMessage();
		else
			return "Your service is ready to be used for calls and SMSs.";
	}
	
	public function blnServiceReady($blnConnectToService = false)
	{
		if(		$this->strBaseUrl == "" ||
				!$this->arrRequestOptions["strAuthenticationUsername"] ||
				!$this->arrRequestOptions["strAuthenticationPassword"] ||
				!$this->arrRequestOptions["cnsSoapVersion"] ||
				$blnConnect && ($objCallfire = objGetSoapClient()) === false)
			return false;
		else
			return true;
	}
	
	public function arrServiceInfo()
	{
		return null;
		return array(
				"strSID" => "",
				"strFriendlyName" => "",
				"strStatus" => "",
				"blnStatus" => "",
				"strType" => "",
				"blnOutboundCallingSupported" => "",
				"blnInboundCallingSupported" => "",
				"blnOutboundSmsSupported" => "",
				"blnInboundSmsSupported" => "",
				"intCreditUsdCents" => "",
				"datCreated" => "",
				"datModified" => "");
	}
	
	
	
	
	
	
	
	


	public function arrAccountInfo()
	{
		return null;
		return array(
				"strSID" => "",
				"strFriendlyName" => "",
				"strStatus" => "",
				"blnStatus" => "",
				"strType" => "",
				"blnOutboundCallingSupported" => "",
				"blnInboundCallingSupported" => "",
				"blnOutboundSmsSupported" => "",
				"blnInboundSmsSupported" => "",
				"intCreditUsdCents" => "",
				"datCreated" => "",
				"datModified" => "");
	}

	public function arrSendMessage($strMessage = null, $strTo = null, $strFrom = null)
	{
		if(!trim($strMessage))
			$this->strLastError = "A message was not provided.";
		elseif(!trim($strTo))
			$this->strLastError = "A receiving number was not provided.";
		elseif(!trim($strFrom) && !$this->arrRequestOptions["strDefaultFromNumber"])
			$this->strLastError = "A sending number was not provided.";
		else 
		{
			if(!trim($strFrom))
				$strFrom = $this->arrRequestOptions["strDefaultFromNumber"];
			
			$objSoapClient = $this->objGetSoapClient();
			
			$arrOptions = array(
					'BroadcastName' => 'Callfire abstractTAPI auto',
					'ToNumber' => $strTo,
					'TextBroadcastConfig' => array('Message' => $strMessage));
			
			try {
				$strBroadCastID = $objSoapClient->sendText($arrOptions);
				$objTextQuery = $objSoapClient->QueryTexts(array('BroadcastId' => $strBroadCastID));
			}
			catch(Exception $e) {
				$this->strLastError = "The service returned an error whilst sending a text: {$e->getMessage()}";
				return false;
			}
			
			
			if(		isset($objTextQuery->TotalResults) &&
					$objTextQuery->TotalResults === 1 &&
					isset($objTextQuery->Text) &&
					$objTextQuery->Text->id != "")
			{
				try {
					$objTextResult = $objSoapClient->GetText(array('Id' => $objTextQuery->Text->id));
				}
				catch(Exception $e) {
					$this->strLastError = "The service returned an error whilst retrieving results: {$e->getMessage()}";
					return false;
				}
					
				return array(
						"strSID" => $this->strEncodeSID($objTextQuery->Text->id),
						"intPriceCents" => (isset($objTextResult->TextRecord) && isset($objTextResult->TextRecord->BilledAmount)) ?
						round(trim($objTextResult->TextRecord->BilledAmount) * 100) : null,
						"strPriceUnit" => "usd",
						"strStatus" => isset($objTextResult->FinalResult) ? strtolower(trim($this->strStatusTranslation(trim($objTextResult->FinalResult)))) :
						(isset($objTextResult->State) ? strtolower(trim($this->strStatusTranslation(trim($objTextResult->State)))) : "queued"),
						"blnIsStateFinal" => isset($objTextResult->State) ? $this->blnIsStatusFinal(trim($objTextResult->State)) : false,
						"datSent" => (isset($objTextResult->TextRecord) && isset($objTextResult->TextRecord->FinishTime)) ?
						date('Y-m-d H:i:s', strtotime(trim($objTextResult->TextRecord->FinishTime))) : null,
						"datCreated" => date('Y-m-d H:i:s', strtotime(trim($objTextResult->Created))),
						"datModified" => date('Y-m-d H:i:s', strtotime(trim($objTextResult->Modified))));
			}
			else
				return false;
		}
		return false;
	}

	public function arrCheckMessage($strMessageSID = null)
	{
		if(!$this->blnIsThisSID($strMessageSID))
			return false;
		else
		{
			$objSoapClient = $this->objGetSoapClient();
			$intTextID = round($this->strDecodeSID($strMessageSID));
			echo "\n\nText message id: {$intTextID}\n\n";
				
			try {
				$objTextResult = $objSoapClient->GetText(array('Id' => $intTextID));
			}
			catch(Exception $e) {
				$this->strLastError = "The service returned an error whilst retrieving results: {$e->getMessage()}";
				return false;
			}

			return array(
					"strSID" => $strMessageSID,
					"intPriceCents" => (isset($objTextResult->TextRecord) && isset($objTextResult->TextRecord->BilledAmount)) ?
							round(trim($objTextResult->TextRecord->BilledAmount) * 100) : null,
					"strPriceUnit" => "usd",
					"strStatus" => isset($objTextResult->FinalResult) ? strtolower(trim($this->strStatusTranslation(trim($objTextResult->FinalResult)))) :
							(isset($objTextResult->State) ? strtolower(trim($this->strStatusTranslation(trim($objTextResult->State)))) : "queued"),
					"blnIsStateFinal" => isset($objTextResult->State) ? $this->blnIsStatusFinal(trim($objTextResult->State)) : false,
					"datSent" => (isset($objTextResult->TextRecord) && isset($objTextResult->TextRecord->FinishTime)) ?
							date('Y-m-d H:i:s', strtotime(trim($objTextResult->TextRecord->FinishTime))) : null,
					"datCreated" => date('Y-m-d H:i:s', strtotime(trim($objTextResult->Created))),
					"datModified" => date('Y-m-d H:i:s', strtotime(trim($objTextResult->Modified))));
		}
	}


	public function arrMakeCall($strTo = null, $strFrom = null, $arrAdditionalOptions = null)
	{
		if(!trim($strTo))
			$this->strLastError = "A receiving number was not provided.";
		if(!trim($strFrom) && !$this->arrRequestOptions["strDefaultFromNumber"])
			$this->strLastError = "A sending number was not provided.";
		else
		{
			if(!trim($strFrom))
				$strFrom = $this->arrRequestOptions["strDefaultFromNumber"];
			
			$objSoapClient = $this->objGetSoapClient();
			$xmlIvrTextToSpeech =
"<dialplan name='Root'>
	<record>
		<play type='tts' voice='female1'>Hello! This is a verification message by checkmyphones dot com to check the status of this number. No further response from your end is needed.</play>
		<hangup/>
	</record>
</dialplan>";
			
			$arrOptions = array(
					'ToNumber' => $strTo,/*
					'VoiceBroadcastConfig' => array(
							'FromNumber' => $strFrom,
							'AnsweringMachineConfig' => 'LIVE_IMMEDIATE',
							'BroadcastName' => 'Callfire abstractTAPI auto',
							'LiveSoundId' => $soundId),*/
					'IvrBroadcastConfig' => array(
							'id' => $this->arrRequestOptions["intBroadcastID"],
							'FromNumber' => $strFrom,
							'DialplanXml' => $xmlIvrTextToSpeech
					)
			);
			try {
				$strBroadCastID = $objSoapClient->sendCall($arrOptions);
					
				$objCallQuery = $objSoapClient->QueryCalls(array('BroadcastId' => $strBroadCastID));
			}
			catch(Exception $e) {
				$this->strLastError = "The service returned an error whilst sending a call: {$e->getMessage()}";
				return false;
			}
			
			if(		isset($objCallQuery->TotalResults) &&
					$objCallQuery->TotalResults === 1 &&
					isset($objCallQuery->Call) &&
					$objCallQuery->Call->id != "")
			{
				try {
					$objCallResult = $objSoapClient->GetCall(array('Id' => $objCallQuery->Call->id));
				}
				catch(Exception $e) {
					$this->strLastError = "The service returned an error whilst retrieving results: {$e->getMessage()}";
					return false;
				}
			
				return array(
						"strSID" => $this->strEncodeSID((string)$objCallQuery->Call->id),
						"strFromNumberSID" => isset($objCallResult->FromNumber) ? $this->strEncodeSID((string)$objCallResult->FromNumber) : "",
						"datCallStart" => (isset($objCallResult->CallRecord) && isset($objCallResult->CallRecord->AnswerTime)) ?
						date('Y-m-d H:i:s', strtotime(trim($objCallResult->CallRecord->AnswerTime))) : null,
						"datCallEnd" => (isset($objCallResult->CallRecord) && isset($objCallResult->CallRecord->FinishTime)) ?
						date('Y-m-d H:i:s', strtotime(trim($objCallResult->CallRecord->FinishTime))) : null,
						"intDurationSeconds" => (isset($objCallResult->CallRecord) && isset($objCallResult->CallRecord->Duration)) ?
						round(trim($objCallResult->CallRecord->Duration)) : null,
						"intPriceCents" => (isset($objCallResult->CallRecord) && isset($objCallResult->CallRecord->BilledAmount)) ?
						round(trim($objCallResult->CallRecord->BilledAmount) * 100) : null,
						"strPriceUnit" => "usd",
						"strResponderType" => null,
						"strForwardedFrom" => null,
						"strAnnotation" => null,
						"strCallerName" => null,
						"strStatus" => isset($objCallResult->FinalResult) ? strtolower(trim($this->strStatusTranslation(trim($objCallResult->FinalResult)))) :
						(isset($objCallResult->State) ? strtolower(trim($this->strStatusTranslation(trim($objCallResult->State)))) : "queued"),
						"blnIsStateFinal" => isset($objCallResult->State) ? $this->blnIsStatusFinal(trim($objCallResult->State)) : false,
						"datCreated" => date('Y-m-d H:i:s', strtotime(trim($objCallResult->Created))),
						"datModified" => date('Y-m-d H:i:s', strtotime(trim($objCallResult->Modified))));
			}
			else
				return false;
		}
		return false;
	}

	public function arrCheckCall($strCallSID = null)
	{
		if(!$this->blnIsThisSID($strCallSID))
			return false;
		else
		{
			$objSoapClient = $this->objGetSoapClient();
			$intCallID = round($this->strDecodeSID($strCallSID));
			
			try {
				$objCallResult = $objSoapClient->GetCall(array('Id' => $intCallID));
			}
			catch(Exception $e) {
				$this->strLastError = "The service returned an error whilst retrieving results: {$e->getMessage()}";
				return false;
			}

			return array(
					"strSID" => $strCallSID,
					"strFromNumberSID" => isset($objCallResult->FromNumber) ? $this->strEncodeSID((string)$objCallResult->FromNumber) : "",
					"datCallStart" => (isset($objCallResult->CallRecord) && isset($objCallResult->CallRecord->AnswerTime)) ? 
							date('Y-m-d H:i:s', strtotime(trim($objCallResult->CallRecord->AnswerTime))) : null,
					"datCallEnd" => (isset($objCallResult->CallRecord) && isset($objCallResult->CallRecord->FinishTime)) ? 
							date('Y-m-d H:i:s', strtotime(trim($objCallResult->CallRecord->FinishTime))) : null,
					"intDurationSeconds" => (isset($objCallResult->CallRecord) && isset($objCallResult->CallRecord->Duration)) ? 
							round(trim($objCallResult->CallRecord->Duration)) : null,
					"intPriceCents" => (isset($objCallResult->CallRecord) && isset($objCallResult->CallRecord->BilledAmount)) ? 
							round(trim($objCallResult->CallRecord->BilledAmount) * 100) : null,
					"strPriceUnit" => "usd",
					"strResponderType" => null,
					"strForwardedFrom" => null,
					"strAnnotation" => null,
					"strCallerName" => null,
					"strStatus" => isset($objCallResult->FinalResult) ? strtolower(trim($this->strStatusTranslation(trim($objCallResult->FinalResult)))) : 
							(isset($objCallResult->State) ? strtolower(trim($this->strStatusTranslation(trim($objCallResult->State)))) : "queued"),
					"blnIsStateFinal" => isset($objCallResult->State) ? $this->blnIsStatusFinal(trim($objCallResult->State)) : false,
					"datCreated" => date('Y-m-d H:i:s', strtotime(trim($objCallResult->Created))),
					"datModified" => date('Y-m-d H:i:s', strtotime(trim($objCallResult->Modified))));
		}
	}
	
	public function arrGetRecordings($strCallSID = null)
	{
		if(!$this->blnIsThisSID($strCallSID))
			return false;
		else
		{
			$objSoapClient = $this->objGetSoapClient();
			$intCallID = round($this->strDecodeSID($strCallSID));

			try {
				$objCallResult = $objSoapClient->GetCall(array('Id' => $intCallID));
			}
			catch(Exception $e) {
				$this->strLastError = "The service returned an error whilst retrieving results: {$e->getMessage()}";
				return false;
			}
			
			if(isset($objCallResult->CallRecord) && isset($objCallResult->CallRecord->RecordingMeta))
			{
				$obrCallRecord = $objCallResult->CallRecord->RecordingMeta;
				return array(
						array(
								"strSID" => isset($obrCallRecord->id) ? $this->strEncodeSID((string)$obrCallRecord->id) : "",
								"intDurationSeconds" => isset($obrCallRecord->id) ? $obrCallRecord->LengthInSeconds : "",
								"datCreated" => date('Y-m-d H:i:s', strtotime(trim($objCallResult->Created))),
								"datModified" => date('Y-m-d H:i:s', strtotime(trim($objCallResult->Modified)))
								)
						);
			}
			else
				return null;
		}
	}

	public function strCreateRecordingUrl($strRecordingSID = null)
	{
		if(!$this->blnIsThisSID(trim($strRecordingSID)))
			return false;
		else
		{
			$intID = $this->strDecodeSID($strRecordingSID);
			return "https://www.callfire.com/ui/sound?recording={$intID}.mp3";
		}
	}

	

	public function arrGetOutboundCallNumbers()
	{
		//robert@mesasix.com
		//flo12345
		
		//return array();
		//TODO: Unfinished code here... Difficult to get this to work
		$objSoapClient = $this->objGetSoapClient();
		try {
			$objResult = $objSoapClient->QueryNumbers(array('Region' => array('City' => 'Dallas'), 'MaxResults' => 10));
			//$objResult = $objSoapClient->QueryNumbers(array('Region' => array('Prefix' => '12022'), 'MaxResults' => 10));
			//$objResult = $objSoapClient->QueryRegions(array('Region' => array('City' => 'Washington'), 'MaxResults' => 10));
			var_dump($objResult);
		}
		catch(Exception $e) {
			$this->strLastError = "The service returned an error whilst retrieving results: {$e->getMessage()}";
			return false;
		}
		
		$arrReturn = array();
		$arrReturn[] = array (
				"strSID" => "",
				"strNumber" => "",
				"blnCallerLookupId" => false,
				"blnOutboundCallingSupported" => false,
				"blnInboundCallingSupported" => false,
				"blnOutboundSmsSupported" => false,
				"blnInboundSmsSupported" => false,
				"datCreated" => "",
				"datModified" => "");
		
		return $arrReturn;
		
	}
	
	public function arrGetInboundCallNumbers()
	{
		return array();
	}
	
	public function arrGetOutboundSmsNumbers()
	{
		//$this->arrRequestOptions["strDefaultServiceNumber"]
		return array();
	}

	public function arrGetInboundSmsNumbers()
	{
		return array();
	}
	
	
	
	private function objGetSoapClient()
	{
		if($this->objSoapClient != null)
			return $this->objSoapClient;
		try
		{
			$this->objSoapClient = new SoapClient($this->strBaseUrl, array(
					'soap_version' => $this->arrRequestOptions["cnsSoapVersion"],
					'login' => $this->arrRequestOptions["strAuthenticationUsername"],
					'password' => $this->arrRequestOptions["strAuthenticationPassword"]));
		}
		catch(SoapFault $objErr)
		{
			$this->strLastError = "There was an error while handshaking the service: {$objErr->getMessage()}";
			return false;
		}
		return $this->objSoapClient;
	}

	private function blnIsStatusFinal($strResponseStatus)
	{
		switch(strtolower($strResponseStatus))
		{
			case "finished":
			case "sent":
				return true;
			default:
				return false;
		}
	}
	
	private function strStatusTranslation($strResponseStatus)
	{
		switch(strtolower($strResponseStatus))
		{
			case "la":
				return "Live Answer";
			case "am":
				return "Answering Machine";
			case "it":
				return "Initial Transfer";
			case "no_ans":
				return "No Answer";
			default:
				return $strResponseStatus;
		}
	}
	
}