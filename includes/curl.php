<?php
/*
 * An array with options for all curl and other related 
 *  variables. Has to be set separately for all services.
 */
$arrRequestOptionsDefault = array(
		"strAuthenticationUsername" => null,
		"strAuthenticationPassword" => null,
		"blnVerifySSL" => false,
		"blnFollowLocation" => true,
		"intMaxRedirects" => 4,
		"intTimeOutSeconds" => 30,
		"blnBinaryTransfers" => false,
		"blnIncludeHeaders" => false,
		"blnNoBody" => false,
		"strUserAgent" => "",
		"strReferer" => "");


/*
 * A function to request the contents of a particular url.
 * 
 * Returns the contents of the url as a string.
 * Returns FALSE on failure.
 * If blnReturnResultsAsArray is true, an array is returned with 
 *  more informatin, such as error description and http status
 */
function strRequestUrl(
		$strUrl = "",
		$arrOptions = array(),
		$arrPostFields = null,
		$blnReturnResultsAsArray = false)
{
	$s = curl_init();

	curl_setopt($s,CURLOPT_RETURNTRANSFER, true);
	
	curl_setopt($s,CURLOPT_URL,$strUrl);
	curl_setopt($s,CURLOPT_HTTPHEADER,array('Expect:'));
	curl_setopt($s,CURLOPT_TIMEOUT,$arrOptions["intTimeOutSeconds"]);
	curl_setopt($s,CURLOPT_MAXREDIRS,$arrOptions["intMaxRedirects"]);
	curl_setopt($s,CURLOPT_RETURNTRANSFER,true);
	curl_setopt($s,CURLOPT_FOLLOWLOCATION,$arrOptions["blnFollowLocation"]);
	curl_setopt($s,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt');
	curl_setopt($s,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt');
	
	if($arrOptions["strAuthenticationUsername"] && $arrOptions["strAuthenticationPassword"]) {
		curl_setopt($s, CURLOPT_USERPWD, $arrOptions["strAuthenticationUsername"].':'.$arrOptions["strAuthenticationPassword"]);
		
		curl_setopt($s, CURLOPT_SSL_VERIFYPEER, $arrOptions["blnVerifySSL"]);
		curl_setopt($s, CURLOPT_SSL_VERIFYHOST, $arrOptions["blnVerifySSL"]);
	}

	if($arrPostFields)
	{
		$strPostFields = "";
		foreach($arrPostFields as $strPostField => $strPostValue)
		{
			$strPostValue = urlencode($strPostValue);
			$strPostFields .= "&{$strPostField}={$strPostValue}";
		}
		
		curl_setopt($s,CURLOPT_POST,count($arrPostFields));
		curl_setopt($s,CURLOPT_POSTFIELDS,$strPostFields);
	}

	if($arrOptions["blnIncludeHeaders"])
		curl_setopt($s,CURLOPT_HEADER,true);

	if($arrOptions["blnNoBody"])
		curl_setopt($s,CURLOPT_NOBODY,true);

	if($arrOptions["blnBinaryTransfers"])
		curl_setopt($s,CURLOPT_BINARYTRANSFER,true);

	curl_setopt($s,CURLOPT_USERAGENT,$arrOptions["strUserAgent"]);
	curl_setopt($s,CURLOPT_REFERER,$arrOptions["strReferer"]);

	$strReturnContents = curl_exec($s);
	$strReturnError = curl_error($s);
	$strReturnStatus = curl_getinfo($s,CURLINFO_HTTP_CODE);
	$strReturnStatusDescription = strHttpCodeDescription($strReturnStatus);
	curl_close($s);
	
	if(!$blnReturnResultsAsArray)
		return $strReturnContents;
	else 
	{
		return array(
				"strContents" => $strReturnContents,
				"strError" => $strReturnError,
				"intHttpStatus" => $strReturnStatus,
				"strHttpStatusDescription" => $strReturnStatusDescription);
	}
}




function strHttpCodeDescription($code)
{
	switch ($code) {
		case 100: $text = 'Continue'; break;
		case 101: $text = 'Switching Protocols'; break;
		case 200: $text = 'OK'; break;
		case 201: $text = 'Created'; break;
		case 202: $text = 'Accepted'; break;
		case 203: $text = 'Non-Authoritative Information'; break;
		case 204: $text = 'No Content'; break;
		case 205: $text = 'Reset Content'; break;
		case 206: $text = 'Partial Content'; break;
		case 300: $text = 'Multiple Choices'; break;
		case 301: $text = 'Moved Permanently'; break;
		case 302: $text = 'Moved Temporarily'; break;
		case 303: $text = 'See Other'; break;
		case 304: $text = 'Not Modified'; break;
		case 305: $text = 'Use Proxy'; break;
		case 400: $text = 'Bad Request'; break;
		case 401: $text = 'Unauthorized'; break;
		case 402: $text = 'Payment Required'; break;
		case 403: $text = 'Forbidden'; break;
		case 404: $text = 'Not Found'; break;
		case 405: $text = 'Method Not Allowed'; break;
		case 406: $text = 'Not Acceptable'; break;
		case 407: $text = 'Proxy Authentication Required'; break;
		case 408: $text = 'Request Time-out'; break;
		case 409: $text = 'Conflict'; break;
		case 410: $text = 'Gone'; break;
		case 411: $text = 'Length Required'; break;
		case 412: $text = 'Precondition Failed'; break;
		case 413: $text = 'Request Entity Too Large'; break;
		case 414: $text = 'Request-URI Too Large'; break;
		case 415: $text = 'Unsupported Media Type'; break;
		case 500: $text = 'Internal Server Error'; break;
		case 501: $text = 'Not Implemented'; break;
		case 502: $text = 'Bad Gateway'; break;
		case 503: $text = 'Service Unavailable'; break;
		case 504: $text = 'Gateway Time-out'; break;
		case 505: $text = 'HTTP Version not supported'; break;
		default:  $text = 'Unknown http status code "' . htmlentities($code) . '"'; break;
	}
}