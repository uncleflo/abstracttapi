<?php
include_once("curl.php");

$strLastJsonError = "";
$intLastJsonStatus = null;

/*
 * A function to assist with interpreting urls that return JSON data.
 * 
 * Returns the service as an array, or returns false on error.
 * 
 * The error can be retrieved using the global 
 */
function arrRequestJSON(
		$strUrl = "",
		$arrRequestOptions = null,
		$arrPostFields = null)
{
	global $strLastJsonError, $intLastJsonStatus;
	
	if($arrRequestOptions == null)
		$arrRequestOptions = $arrRequestOptionsDefault;
	
	$intLastJsonStatus = null;
	$strLastJsonError = "";
	$arrRequestResults = strRequestUrl(
		$strUrl,
		$arrRequestOptions,
		$arrPostFields,
		true);
	$intLastJsonStatus = (int)$arrRequestResults["intHttpStatus"];

	if($arrRequestResults["strError"] !== "")
		$strLastJsonError = "There was an error communicating with the messaging service: {$arrRequestResults["strError"]}";
	/*elseif(!($arrRequestResults["intHttpStatus"] >= 200 && $arrRequestResults["intHttpStatus"] < 300))
		$strLastJsonError = "The http response code is not successful: {$arrRequestResults["intHttpStatus"]}: {$arrRequestResults["strHttpStatusDescription"]}";*/
	elseif(($strContent = $arrRequestResults["strContents"]) === false)
		$strLastJsonError = "There was no data returned from the messaging service, the event might have failed.";
	elseif(($arrContent = json_decode($strContent, true)) === null && ($intJsonLastError = json_last_error()) !== JSON_ERROR_NONE)
		$strLastJsonError = "There was an error with interpreting the returned data from the service: " . json_last_error_description($intJsonLastError);
	else
		return $arrContent;
	return false;
}

function strRequestJSONError()
{
	global $strLastJsonError;
	return $strLastJsonError;
}

function intRequestJSONStatus()
{
	global $intLastJsonStatus;
	return $intLastJsonStatus;
}



//Some support functions, including circumventing old php versions...
function json_last_error_description($intJSONErrorCode)
{
	$arrErrDescriptions = array(
			JSON_ERROR_NONE => "No error has occurred",
			JSON_ERROR_DEPTH => "The maximum stack depth has been exceeded",
			JSON_ERROR_STATE_MISMATCH => "Invalid or malformed JSON",
			JSON_ERROR_CTRL_CHAR => "Control character error, possibly incorrectly encoded",
			JSON_ERROR_SYNTAX => "Syntax error",
			JSON_ERROR_UTF8 => "Malformed UTF-8 characters, possibly incorrectly encoded");
	if(in_array($intJSONErrorCode, $arrErrDescriptions))
		return $arrErrDescriptions[$intJSONErrorCode];
	else
		return "Unknown error";
}

if (!version_compare(PHP_VERSION, '5.3.3', '>='))
{
	define("JSON_ERROR_NONE", 0);
	define("JSON_ERROR_DEPTH", 1);
	define("JSON_ERROR_STATE_MISMATCH", 2);
	define("JSON_ERROR_CTRL_CHAR", 3);
	define("JSON_ERROR_SYNTAX", 4);
	define("JSON_ERROR_UTF8", 5);
	function json_last_error()
	{
		return 0;
	}
}