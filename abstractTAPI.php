<?php
set_include_path(get_include_path() . PATH_SEPARATOR . dirname(__FILE__));

function objAbstractTapiGetInstance($strServiceName = "", $arrServiceConfigs = array())
{
	if(!isset($arrServiceConfigs[$strServiceName]))
		return null;
	else
	{
		include_once("{$strServiceName}/{$strServiceName}.php");
		$strClassName = "cls" . ucfirst(strtolower(str_replace(".", "_", $strServiceName))) . "API";
		$objReturn = new $strClassName($arrServiceConfigs[$strServiceName]);
		return $objReturn;
	}
}
