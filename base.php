<?php

define("ABSTRACTTAPI_BASE", "BASE");

/*
 * The base class, that each service tapi should extend to. This is the class that should be aimed to be extendeable.
 * 
 * Here are some of the responses that each function implementation should expect:
 * Service does not support the function: Return NULL
 * Service returns an error: Return FALSE (Check last error message for more info)
 * Service call is successful: Returns true or an array, structured as specifically to the function.
 */
abstract class clsTelephonyAPI
{
	var $strBaseUrl;
	var $arrRequestOptions;
	var $strService;


	var $strLastError;
	/*
	 * Error handling class. Prefer NOT to use this, all errors should be 
	 *  thrown as administrator friendly strings.
	 */
	public function strLastErrorMessage()
	{
		return $this->strLastError;
	}
	
	/*
	 * Construct and destruct functions. See construct info.
	 */
	public function __construct($arrOptions = array())
	{
		return $this->blnConnect($arrOptions);
	}

	public function __destruct()
	{
		return $this->blnDisconnect();
	}

	/*
	 * Function to connect to the service. All extended service classes 
	 *  use a single Option array, this allows better interchangeability
	 *  when using multiple service classes at the same time.
	 *  
	 *  Options variable contains service class specific options.
	 */
	public function blnConnect($arrOptions = array())
	{}


	public function blnDisconnect()
	{}
	
	/*
	 * A beginners friendly assistance function to check if the service 
	 *  is working properly, and to advice the developer or administrator
	 *  which problems there may be why a call or message could not be 
	 *  sent.
	 * 
	 * Putting blnConnectToService to true, will also check with the service.
	 * 
	 * Returns an (administrator friendly) string.
	 */
	public function strAdviceCheck($blnConnectToService = false)
	{
		return "No service class is loaded. You need to extend this class with " .
			"a specific service class, and instantiate an object from that.";
	}
	
	/*
	 * A more programmatic method to check if a service is fully ready to 
	 *  make calls or send messages. Does almost the same checks as above.
	 * 
	 * Returns true if service is ready to make/receive calls and sent SMSs.
	 */
	public function blnServiceReady($blnConnectToService = false)
	{
		return false;
	}

	/*
	 * Get info about the service.
	 * 
	 * Returns an array with following indexes:
	 *  strSID
	 *  strFriendlyName
	 *  strStatus
	 *  blnStatus
	 *  strType
	 *  blnOutboundCallingSupported
	 *  blnInboundCallingSupported
	 *  blnOutboundSmsSupported
	 *  blnInboundSmsSupported
	 *  intCreditUsdCents
	 *  datCreated
	 *  datModified
	 */
	public function arrServiceInfo()
	{
		return null;
		return array(
				"strSID" => "",
				"strFriendlyName" => "",
				"strStatus" => "",
				"blnStatus" => "",
				"strType" => "",
				"blnOutboundCallingSupported" => "",
				"blnInboundCallingSupported" => "",
				"blnOutboundSmsSupported" => "",
				"blnInboundSmsSupported" => "",
				"intCreditUsdCents" => "",
				"datCreated" => "",
				"datModified" => "");
	}
	
	
	
	
	
	
	/*
	 * Encode the service ID of a particular record. The encoded SID should 
	 *  identify which service (-class) used, as well as the record id within 
	 *  that service. This is a support function. 
	 * Not sure to place this outside the class.
	 * 
	 * Returns a string.
	 */
	public function strEncodeSID($strID = null, $strService = null)
	{
		if($strID !== null)
		{
			$strID = trim($strID);
			//To allow static calls to this function
			if($strService !== null)
			{
				$strService = trim($strService);
				return "{$strService}*{$strID}";
			}
			else
				return "{$this->strService}*{$strID}";
		}
		else
			return null;
	}

	/*
	 * Decode an encoded service ID. This is a support function. 
	 * Not sure to place this outside the class.
	 * 
	 * Returns the SID as a string
	 */
	public function strDecodeSID($strSID, $blnReturnArray = false)
	{
		if($strSID === null)
			return false;
		$strSID = trim($strSID);
		
		$strService = trim(strtok($strSID, '*'));
		$strID = trim(substr($strSID, strlen($strService) + 1, strlen($strSID) - (strlen($strService) + 1)));
		$arrResult = array(
						"strService" => $strService,
						"strID" => $strID);
		
		//Adding compatibility for static functions
		if($blnReturnArray !== false)
			return $arrResult;
		else
			return $strID;
	}
	

	public function blnIsSID($strSID)
	{
		$strService = trim(strtok($strSID, '*'));
		if($this->blnIsValidDomainName($strService))
		{
			if(strlen($strSID) - (strlen($strService) + 1) > 0)
				return true;
			else
				return false;
		}
		else
			return false;
	}
	
	public function blnIsThisSID($strSID)
	{
		$strService = trim(strtok($strSID, '*'));
		if(!$this->blnIsSID($strSID))
		{
			return false;
		}
		if($strService != $this->strService)
		{
			return false;
		}
		else
			return true;
	}
	
	private function blnIsValidDomainName($strDomainName)
	{
		return (preg_match("/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i", $strDomainName) //valid chars check
				&& preg_match("/^.{1,253}$/", $strDomainName) //overall length check
				&& preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", $strDomainName)   ); //length of each label
	}
	
	


	public function arrAccountInfo()
	{
		return null;
		return array(
				"strSID" => "",
				"strFriendlyName" => "",
				"strStatus" => "",
				"blnStatus" => "",
				"strType" => "",
				"blnOutboundCallingSupported" => "",
				"blnInboundCallingSupported" => "",
				"blnOutboundSmsSupported" => "",
				"blnInboundSmsSupported" => "",
				"intCreditUsdCents" => "",
				"datCreated" => "",
				"datModified" => "");
	}

	public function arrSendMessage($strMessage = null, $strTo = null, $strFrom = null)
	{
		return null;
		return array(
				"strSID" => "",
				"intPriceCents" => 0,
				"strPriceUnit" => "",
				"strStatus" => "",
				"blnIsStateFinal" => false,
				"datSent" => "",
				"datCreated" => "",
				"datModified" => "");
	}

	public function arrCheckMessage($strMessageSID = null)
	{
		return null;
		return array(
				"strSID" => "",
				"intPriceCents" => 0,
				"strPriceUnit" => "",
				"strStatus" => "",
				"blnIsStateFinal" => false,
				"datSent" => "",
				"datCreated" => "",
				"datModified" => "");
	}


	public function arrMakeCall($strTo = null, $strFrom = null, $arrAdditionalOptions = null)
	{
		return null;
		return array(
				"strSID" => "",
				"strFromNumberSID" => "",
				"datCallStart" => "",
				"datCallEnd" => "",
				"intDurationSeconds" => 0,
				"intPriceCents" => 0,
				"strPriceUnit" => "",
				"strResponderType" => "",
				"strForwardedFrom" => "",
				"strAnnotation" => "",
				"strCallerName" => "",
				"strStatus" => "",
				"blnIsStateFinal" => false,
				"datCreated" => "",
				"datModified" => "");
	}

	public function arrCheckCall($strCallSID = null)
	{
		return null;
		return array(
				"strSID" => "",
				"strFromNumberSID" => "",
				"datCallStart" => "",
				"datCallEnd" => "",
				"intDurationSeconds" => 0,
				"intPriceCents" => 0,
				"strPriceUnit" => "",
				"strResponderType" => "",
				"strForwardedFrom" => "",
				"strAnnotation" => "",
				"strCallerName" => "",
				"strStatus" => "",
				"blnIsStateFinal" => false,
				"datCreated" => "",
				"datModified" => "");
	}


	public function arrGetRecordings($strCallSID = null)
	{
		return null;
		return array(
				array(
						"strSID" => "",
						"intDurationSeconds" => "",
						"datCreated" => "",
						"datModified" => ""),
				array(
						"strSID" => "",
						"intDurationSeconds" => "",
						"datCreated" => "",
						"datModified" => "")
				);
	}

	public function strCreateRecordingUrl($strRecordingSID = null)
	{
		return "";
	}

	

	public function arrGetOutboundCallNumbers()
	{
		return null;
		return array(
				"strSID" => "",
				"strNumber" => "",
				"blnCallerLookupId" => false,
				"blnOutboundCallingSupported" => false,
				"blnInboundCallingSupported" => false,
				"blnOutboundSmsSupported" => false,
				"blnInboundSmsSupported" => false,
				"datCreated" => "",
				"datModified" => "");
	}
	
	public function arrGetInboundCallNumbers()
	{
		return null;
		return array(
				"strSID" => "",
				"strNumber" => "",
				"blnCallerLookupId" => false,
				"blnOutboundCallingSupported" => false,
				"blnInboundCallingSupported" => false,
				"blnOutboundSmsSupported" => false,
				"blnInboundSmsSupported" => false,
				"datCreated" => "",
				"datModified" => "");
	}
	
	public function arrGetOutboundSmsNumbers()
	{
		return null;
		return array(
				"strSID" => "",
				"strNumber" => "",
				"blnCallerLookupId" => false,
				"blnOutboundCallingSupported" => false,
				"blnInboundCallingSupported" => false,
				"blnOutboundSmsSupported" => false,
				"blnInboundSmsSupported" => false,
				"datCreated" => "",
				"datModified" => "");
	}

	public function arrGetInboundSmsNumbers()
	{
		return null;
		return array(
				"strSID" => "",
				"strNumber" => "",
				"blnCallerLookupId" => false,
				"blnOutboundCallingSupported" => false,
				"blnInboundCallingSupported" => false,
				"blnOutboundSmsSupported" => false,
				"blnInboundSmsSupported" => false,
				"datCreated" => "",
				"datModified" => "");
	}
}